CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Configuration
 * How to use
 * Credits


INTRODUCTION
------------

Nepali calendar module converts Gregorian calendar (A.D.)
to Bikram Sambat (B.S.) and vice-versa.

The official Nepali calendar follows Bikram Sambat,
abbreviated B.S., said to havebeen started by mythical Indian
emperor Vikramāditya. The B.S. year is approximately
56.7 years ahead of Gregorian calendar. The Gregorian year
2000 AD began 17 Poush 2056 and ended 16 Poush 2057.


INSTALLATION
------------

1. Copy the nepali_calendar folder to the modules folder
   in your installation.

2. Enable the module using Administration -> Modules
   (/admin/modules)


CONFIGURATION
-------------

* Go to
  » Administration
  » Configuration
  » Regional and language
  » Nepali calendar.

* Make necessary settings for Nepali date and time.


HOW TO USE
----------

Nepali date block is created automatically when
"Nepali calendar" module is installed. This block contains a
converted Nepali date and Nepal time
(e.g.,Date: Friday, Falgun 29, 2067 08:00 pm).
Users can assign this block to a region where Nepali date and
time is supposed to be displayed.


CREDITS
-------
  - Nepali calendar module for Drupal is developed by Chhabi Pachabhaiya.
      http://c.pachabhaiya.com
      https://drupal.org/user/2604444
